import pynvml
import time

def main():
    pynvml.nvmlInit()
    nvmhandle = pynvml.nvmlDeviceGetHandleByIndex(0) 
    print(pynvml.nvmlDeviceGetName(nvmhandle))
    print(pynvml.nvmlDeviceGetMemoryInfo(nvmhandle).total)
    average_power = 0
    alpha = 0.001
    first_measurement = True

    while True: 
        power_usage = pynvml.nvmlDeviceGetPowerUsage(nvmhandle) / 1000.0  # Convert milliwatts to watts
        print('power_usage', power_usage)
        if first_measurement:
            average_power = power_usage  # Start with the first measurement
            first_measurement = False
        else:
            average_power = alpha * power_usage + (1 - alpha) * average_power  # Update the EMA
        time.sleep(1) 
        print('average_power', average_power)

if __name__ == "__main__":
    main()