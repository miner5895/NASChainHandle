from datetime import datetime
import time
import sys
import requests
import os

tele_chat_id = '-4109442957'
tele_token = '1636897847:AAGEVQWX5LqhJBv34kGcukBJ__JwZkOYfrs'


def tail(file_path, lines=50):
    with open(file_path, 'r') as file:
        lines_buffer = file.readlines()
        return lines_buffer[-lines:]


def checkRunning():
    try:
        tmp_path = sys.argv[1].replace("_", "-")
        path = f"/root/.pm2/logs/{tmp_path}-out.log"
        timediff = 86400
        for line in tail(path, 50):
            if 'Requsting Job from Genomaster' in line or 'train_acc' in line or 'Cho thoi gian tra ket qua' in line or 'Axon created' in line or 'Get job' in line or 'get_server_result' in line or 'Kiem tra so job hien tai' in line:
                line = line.replace('\x1b[34m', '')
                timeStr = line[0:19]
                dt_object = datetime.strptime(timeStr, '%Y-%m-%d %H:%M:%S')
                successTimestamp = int(dt_object.timestamp())
                timeNow = int(time.time())
                timediff = timeNow - successTimestamp

        print(f"{path} Last run: {timediff}")
        if timediff > 120:
            os.system(f"pm2 restart {sys.argv[1]}")
            data = {
                "chat_id": tele_chat_id,
                "text": f"{path} thanh cong cach day {timediff} giay",
                "parse_mode": "HTML"
            }
            requests.post(
                f'https://api.telegram.org/bot{tele_token}/sendMessage',
                json=data)

    except Exception as e:
        print("Co loi xay ra")
        print(e)


def main():
    while True:
        checkRunning()
        for i in range(30):
            print(f"Con lai {30 - i}s")
            time.sleep(1)
            
if __name__ == "__main__":
    main()