import sys
sys.path.insert(0, 'nsga-net/')
from search.train_search import TrainSearch


if __name__ == "__main__":
    # Job 001001101100000010101101010010001000000101010000000001000000000000 results submitted by 170. Responses: [89.93, 0.79543, 175.4584]
    job = {
        'Genome_String': '000000100000001101111110110111001110000110100011111001110100000101011011111001000100111001011000000010100000000010100000000000010000100000'    }
    splitLength = len(job['Genome_String']) / 3
    elements = [int(char) for char in job['Genome_String']]
    Genome = [elements[i:i + 46] for i in range(0, len(elements), 46)]
    config = {
        'Genome': Genome,
        'Genome_String': job['Genome_String'],
        'config': {
            "epochs": 45,
            "init_channels": 36,
            "layers": 30,
            "search_space": "macro",
            "seed": 0
        }}

    print('config', config)

    train_search = TrainSearch(1)
    performance = train_search.main(genome=config['Genome'],
                                    search_space=config['config']['search_space'],
                                    init_channels=config['config']['init_channels'],
                                    layers=config['config']['layers'], cutout=False,
                                    epochs=config['config']['epochs'],
                                    save='arch_{}'.format(1),
                                    expr_root='')
    train_res = list(performance.values())
    print('train_res', train_res)
