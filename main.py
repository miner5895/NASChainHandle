import sys
sys.path.insert(0, 'nsga-net/')
from search.train_search import TrainSearch
import uvicorn
from fastapi import FastAPI
import requests
import time
import json
import os
import bittensor as bt
from dotenv import dotenv_values
import pynvml
from threading import Thread

class Miner:
    def __init__(self):
        self.version = "0.0.1"
        try:
            # Initialize NVML
            pynvml.nvmlInit()
            self.nvmhandle = pynvml.nvmlDeviceGetHandleByIndex(int(sys.argv[2])) 
        except Exception as e:
            bt.logging.error(f'❌ Error Init nvm {e}')

        self.average_power = 0

    def monitor_gpu_power(self):
        """
        Monitors GPU power consumption continuously until `should_exit` is set to True.
        """
        alpha = 0.001  # Smoothing factor
        first_measurement = True
        try:
            while True: 
                power_usage = pynvml.nvmlDeviceGetPowerUsage(self.nvmhandle) / 1000.0  # Convert milliwatts to watts
                if first_measurement:
                    self.average_power = power_usage  # Start with the first measurement
                    first_measurement = False
                else:
                    self.average_power = alpha * power_usage + (1 - alpha) * self.average_power  # Update the EMA
                time.sleep(1) 
        except Exception as e:
            bt.logging.error(f"An error occurred: {e}")
        finally:
            pynvml.nvmlShutdown()  # Ensure NVML shutdown if an error occurs or loop is manually stopped

    def run(self):
        self.gpu_monitoring_thread = Thread(target=self.monitor_gpu_power, daemon=True)
        self.gpu_monitoring_thread.start()

        try:
            with open("ip.txt", "r") as file:
                device = file.read()
        except FileNotFoundError:
            device = ""

        if device == "":
            device = sys.argv[1]
            
        while True:
            env_config = dotenv_values(".env")
            try:
                bt.logging.info(f"[{device}:{sys.argv[2]}] Get job...")

                response = requests.get(f"{env_config['MINER_URL']}/get-job?device={device}:{sys.argv[2]}", timeout=10)
                dataAPI = response.json()
                if "job_id" in dataAPI:
                    Genome_String = dataAPI["job_id"]
                    bt.logging.info(f"Handle job: {Genome_String}")

                    response_result = requests.request("POST", f"{env_config['MINER_URL']}/update-job", headers={
                        'Content-Type': 'application/json'
                        }, data=json.dumps({
                            "id": dataAPI['id'],
                            "step": 0
                        }))

                    splitLength = int(len(Genome_String) / 3)
                    elements = [int(char) for char in Genome_String]
                    Genome = [elements[i:i + splitLength] for i in range(0, len(elements), splitLength)]

                    if 'data' in dataAPI and dataAPI['data'] is not None:
                        config = dataAPI['data']

                        print('config', config)

                        train_search = TrainSearch(1)
                        performance = train_search.main(genome=config['Genome'],
                                                        search_space=config['config']['search_space'],
                                                        init_channels=config['config']['init_channels'],
                                                        layers=config['config']['layers'], cutout=config['config']['cutout'],
                                                        epochs=config['config']['epochs'],
                                                        save='arch_{}'.format(1),
                                                        gpu=int(sys.argv[2]),
                                                        expr_root='')
                        train_res = list(performance.values())
                        train_res.append(self.average_power)
                        train_res.append(pynvml.nvmlDeviceGetName(self.nvmhandle))
                        train_res.append(pynvml.nvmlDeviceGetMemoryInfo(self.nvmhandle).total)
                        train_res.append(self.version)

                        print('train_res', train_res)
                        bt.logging.info(f"Send result job: {Genome_String}")
                        response_result = requests.request("POST", f"{env_config['MINER_URL']}/update-job", headers={
                            'Content-Type': 'application/json'
                            }, data=json.dumps({
                                "id": dataAPI['id'],
                                "result": train_res
                            }))
                        print('Result', json.dumps(response_result.json(), indent=4))
                    else:
                        print('Không tìm thấy data')
                    time.sleep(15)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(str(e), exc_type, fname, exc_tb.tb_lineno)
            
            time.sleep(1)

if __name__ == "__main__":
    miner = Miner()
    miner.run()
