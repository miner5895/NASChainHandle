#!/bin/bash

for ((i=0; i<=$1; i++))
do
    pm2 start python3 --name "server${i}" -- main.py "$2" "$i"
done